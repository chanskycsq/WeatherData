package common

import java.text.DecimalFormat

import org.joda.time.format.DateTimeFormat

import scala.util.Random

/**
  * Created by louischen on 18/09/2016.
  */
class WeatherRecord(random: Random, station: Station) {

  val city = station.city
  val latlon = station.latlon

  val date_ran = System.currentTimeMillis() + random.nextInt()
  val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
  val dateTime_ran = dateTimeFormatter.print(date_ran)

  val decimalFormat = new DecimalFormat("+#.#;-#.#")
  val temperature_ran = BigDecimal(-10 + (40 + 10) * random.nextDouble()).setScale(1, BigDecimal.RoundingMode.HALF_UP).toDouble
  val temperature_fmt = decimalFormat.format(temperature_ran)
  val humidity_ran = random.nextInt(100)
  val pressure_ran = BigDecimal(500 + (1500 - 500) * random.nextDouble()).setScale(1, BigDecimal.RoundingMode.HALF_UP).toDouble

  def getCondition(t: Double, h: Int): String = {
    (t, h) match {
      case (t, h) if t >= 0 && h >= 50 => "Rain"
      case (t, h) if t < 0 && h >= 50 => "Snow"
      case _ => "Sunny"
    }
  }

  val condition_ran = getCondition(temperature_ran, humidity_ran)
}
