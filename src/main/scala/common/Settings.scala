package common


import com.typesafe.config.{Config, ConfigFactory}
/**
  * Created by louischen on 18/09/2016.
  */
class Settings(config: Config) {

  private final val CITY = "city"
  private final val LATLON = "latlon"

  def entityConfiguration(station: String) : EntityConfiguration = {
    val entityConfig = config.getConfig("stations").getConfig(station)
    val city = entityConfig.getString(CITY)
    val latlon = entityConfig.getString(LATLON)
    new EntityConfiguration(city,latlon)
  }

}

object Settings {
  private[this] lazy val config = ConfigFactory.load.getConfig("weather")

  def load = new Settings(config)
}
