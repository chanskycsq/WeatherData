package common

/**
  * Created by louischen on 18/09/2016.
  */
class Station(config: EntityConfiguration) {
  val city = config.city
  val latlon = config.latlon
}
