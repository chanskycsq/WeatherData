package tasks


import common.{Station, Settings, WeatherRecord}

import scala.util.Random


/**
  * Created by louischen on 18/09/2016.
  */
object GeneratorPlausibleWeather {
  def main(args: Array[String]): Unit = {

    val stations = Array("sydney", "melbourne", "adelaide",
      "brisbane", "newcastle", "geelong",
      "hobart", "darwin", "perth",
      "goldcoast", "canberra")

    val settings: Settings = Settings.load

    1 to 500 foreach { _ => output }
    def output = {
      val station_ran = Random.shuffle(stations.toList).head
      val station = new Station(settings.entityConfiguration(station_ran))
      val random = new Random()
      val r = new WeatherRecord(random, station)
      println(r.city
        + '|' + r.latlon
        + '|' + r.dateTime_ran
        + '|' + r.condition_ran
        + '|' + r.temperature_fmt
        + '|' + r.pressure_ran
        + '|' + r.humidity_ran)
    }
  }
}
