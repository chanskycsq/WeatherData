package common


import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random


/**
  * Created by louischen on 19/09/2016.
  */
class WeatherRecordTest extends FlatSpec with Matchers {

  "Temperature" should "be >= -10 and <= 40" in {
    val random = new Random()
    val settings: Settings = Settings.load
    val stations = Array("sydney", "melbourne", "adelaide",
      "brisbane", "newcastle", "geelong",
      "hobart", "darwin", "perth",
      "goldcoast", "canberra")
    val station_ran = Random.shuffle(stations.toList).head
    val station = new Station(settings.entityConfiguration(station_ran))
    val weatherRecord = new WeatherRecord(random, station)
    val result = weatherRecord.temperature_ran.toDouble
    result should be <= 40.00
    result should be >= -10.00
  }

  "Humidity" should "be >= 0 and <= 100" in {
    val random = new Random()
    val settings: Settings = Settings.load
    val stations = Array("sydney", "melbourne", "adelaide",
      "brisbane", "newcastle", "geelong",
      "hobart", "darwin", "perth",
      "goldcoast", "canberra")
    val station_ran = Random.shuffle(stations.toList).head
    val station = new Station(settings.entityConfiguration(station_ran))
    val weatherRecord = new WeatherRecord(random, station)
    val result = weatherRecord.humidity_ran.toInt
    result should be <= 100
    result should be >= 0
  }

  "Pressure" should "be >= 500 and <= 1500" in {
    val random = new Random()
    val settings: Settings = Settings.load
    val stations = Array("sydney", "melbourne", "adelaide",
      "brisbane", "newcastle", "geelong",
      "hobart", "darwin", "perth",
      "goldcoast", "canberra")
    val station_ran = Random.shuffle(stations.toList).head
    val station = new Station(settings.entityConfiguration(station_ran))
    val weatherRecord = new WeatherRecord(random, station)
    val result = weatherRecord.condition_ran
    result should fullyMatch regex "Rain|Snow|Sunny"
  }

  "Condition" should "be Sunny or Rain or Snow" in {
    val random = new Random()
    val settings: Settings = Settings.load
    val stations = Array("sydney", "melbourne", "adelaide",
      "brisbane", "newcastle", "geelong",
      "hobart", "darwin", "perth",
      "goldcoast", "canberra")
    val station_ran = Random.shuffle(stations.toList).head
    val station = new Station(settings.entityConfiguration(station_ran))
    val weatherRecord = new WeatherRecord(random, station)
    val result = weatherRecord.pressure_ran.toDouble
    result should be <= 1500.00
  }
}