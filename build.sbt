
name := "PlausibleWeatherGenerator"

version := "1.0"

scalaVersion := "2.11.8"
scalacOptions += "-feature"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.2.1",
  "joda-time" % "joda-time" % "2.9.4",
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)

coverageEnabled.in(Test, test) := true
coverageExcludedPackages := "tasks"
coverageMinimum := 60
coverageFailOnMinimum := true

parallelExecution in Test := false


test in assembly := {}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
